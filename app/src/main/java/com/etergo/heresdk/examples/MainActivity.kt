package com.etergo.heresdk.examples

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import com.here.android.mpa.common.ApplicationContext
import com.here.android.mpa.common.MapEngine
import com.here.android.mpa.common.OnEngineInitListener
import com.here.android.mpa.common.Version
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.mapping.OnMapRenderListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var map: Map
    private val TAG = MainActivity::class.java.simpleName

    private val constraintsNarrow = ConstraintSet()
    private val constraintsWide = ConstraintSet()

    private var wideView = true

    private val changeBounds = ChangeBounds().apply {
        duration = 300
        interpolator = AccelerateDecelerateInterpolator()
    }

    private val mapRenderListener = object: OnMapRenderListener.OnMapRenderListenerAdapter() {
        override fun onSizeChanged(width: Int, height: Int) {
            super.onSizeChanged(width, height)
            Log.d(TAG, "onSizeChanged: width=$width, height=$height")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupAnimations()
        Log.d(TAG, "HERE SDK version: ${Version.getSdkVersion()}")
        MapEngine.getInstance().init(ApplicationContext(this)) {
            if(it == OnEngineInitListener.Error.NONE) {
                map = Map()
                example_map_view.map = map
                example_map_view.addOnMapRenderListener(mapRenderListener)
            }
        }
    }

    private fun setupAnimations() {
        constraintsWide.clone(root_constraint_layout)
        constraintsNarrow.apply {
            clone(root_constraint_layout)

            clear(R.id.example_map_view, ConstraintSet.START)
            constrainWidth(R.id.example_map_view, 400)
        }

        btn_toggle.setOnClickListener {
            wideView = !wideView
            val message = if(wideView) "Wide" else "Narrow"
            Log.d(TAG, "Clicked Toggle; changing to $message view")
            val targetConstraints = if(wideView) constraintsWide else constraintsNarrow
            TransitionManager.beginDelayedTransition(root_constraint_layout, changeBounds)
            targetConstraints.applyTo(root_constraint_layout)
        }
    }

    override fun onResume() {
        super.onResume()
        example_map_view.onResume()
    }

    override fun onPause() {
        super.onPause()
        example_map_view.onPause()
    }
}
