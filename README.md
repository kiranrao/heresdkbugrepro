
## Introduction

This repo demonstrates some performance issues with using HERE Android SDK Premium v3.10. Specifically, it is related to visually poor performance when *resizing a MapView*. These issues are not seen when using HERE SDK 3.9.

## Instructions

1. Place the HERE-sdk.aar in `app/libs` folder
2. Add HERE SDK credentials anywhere in strings.xml. Specifically, create 3 strings with ids `here_appid`, `here_apptoken` and `here_license_key` 
3. Run the app

It is suggested to run in *landscape orientation*. Tested on an emulator with 7" WSVGA Tablet configuration, 1024x600 resolution; API level 27.

## What does the app do?

The app simply displays a HERE SDK MapView. By default, it displays full screen. Clicking on the "Toggle" button shows a narrower version of the same map (hardcoded to 400 px).

*Crucially*, we use Android Transition framework to animate the size change.

## What is the problem?

The resize has visual stutter. You can see a huge delay between when you click and when the map starts animating. Further, there are what seem to be frame drops in the map during the animation.

### Logs and observations

To help debug the issue, I added logs - specifically `onSizeChanged`. Some interesting logs are attached below. 

#### HERE SDK version: 3.10.0.96

```
13:20:13.003 D: Clicked Toggle; changing to Narrow view
13:20:13.437 D: onSizeChanged: width=400, height=464
13:20:13.440 D: onSizeChanged: width=1024, height=464
13:20:13.657 D: onSizeChanged: width=1018, height=464
13:20:14.075 D: onSizeChanged: width=508, height=464
13:20:14.079 D: onSizeChanged: width=400, height=464

13:20:15.548 D: Clicked Toggle; changing to Wide view
13:20:15.965 D: onSizeChanged: width=1024, height=464
13:20:15.968 D: onSizeChanged: width=400, height=464
13:20:16.178 D: onSizeChanged: width=405, height=464
13:20:16.611 D: onSizeChanged: width=913, height=464
13:20:16.615 D: onSizeChanged: width=1024, height=464
```

There are 2 problems that stand out:

1. There is an initial lag between when the button is clicked and when `onSizeChanged is called` (~400 ms)
2. The width seems to jump to the final value (400) and then again to initial value (1024). After this it starts "animating" to the final value.

For comparison, see the similar logs with HERE SDK 3.9 in the next section. 

#### HERE SDK version: 3.9.0.99

```
13:46:44.830 D: Clicked Toggle; changing to Narrow view
13:46:44.857 D: onSizeChanged: width=1024, height=464
13:46:44.882 D: onSizeChanged: width=1018, height=464
13:46:44.922 D: onSizeChanged: width=1003, height=464
13:46:44.927 D: onSizeChanged: width=977, height=464
13:46:44.951 D: onSizeChanged: width=944, height=464
13:46:44.975 D: onSizeChanged: width=854, height=464
13:46:45.002 D: onSizeChanged: width=802, height=464
13:46:45.021 D: onSizeChanged: width=745, height=464
13:46:45.043 D: onSizeChanged: width=689, height=464
13:46:45.084 D: onSizeChanged: width=579, height=464
13:46:45.117 D: onSizeChanged: width=452, height=464
13:46:45.160 D: onSizeChanged: width=408, height=464
13:46:45.214 D: onSizeChanged: width=400, height=464

13:46:46.558 D: Clicked Toggle; changing to Wide view
13:46:46.582 D: onSizeChanged: width=400, height=464
13:46:46.623 D: onSizeChanged: width=405, height=464
13:46:46.643 D: onSizeChanged: width=445, height=464
13:46:46.677 D: onSizeChanged: width=478, height=464
13:46:46.685 D: onSizeChanged: width=521, height=464
13:46:46.706 D: onSizeChanged: width=567, height=464
13:46:46.726 D: onSizeChanged: width=622, height=464
13:46:46.746 D: onSizeChanged: width=676, height=464
13:46:46.764 D: onSizeChanged: width=790, height=464
13:46:46.792 D: onSizeChanged: width=842, height=464
13:46:46.810 D: onSizeChanged: width=893, height=464
13:46:46.843 D: onSizeChanged: width=972, height=464
13:46:46.868 D: onSizeChanged: width=998, height=464
13:46:46.889 D: onSizeChanged: width=1024, height=464

```

Observations:
1. There is no initial jump to the final value.
2. The size changes smoothly and quickly to the final value (less than 400 ms for the entire animation; compared with over 1000 ms in case of HERE SDK 3.10)


The problem is even more visible if you rapidly click the toggle button before a previous resizing has ended.